package com.atlassian.webdriver.browsers;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.UserAgentPage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestAutoBrowserInstaller extends AbstractSimpleServerTest
{
    UserAgentPage userAgentPage;

    @Before
    public void init()
    {
        userAgentPage = product.getPageBinder().navigateToAndBind(UserAgentPage.class);
    }

    @Test
    @RequireBrowser(Browser.CHROME)
    public void testChrome() throws Exception
    {
        String formattedError = String.format("The user agent: '%s' does not contain 'Chrome/'",
            userAgentPage.getUserAgent());
        assertTrue(formattedError, userAgentPage.getUserAgent().contains("Chrome/"));
    }

    @Test
    @RequireBrowser(Browser.FIREFOX)
    public void testFirefox() throws Exception
    {
        String formattedError = String.format("The user agent: '%s' does not contain 'Firefox/'",
            userAgentPage.getUserAgent());
        assertTrue(formattedError, userAgentPage.getUserAgent().contains("Firefox/"));
    }
}
