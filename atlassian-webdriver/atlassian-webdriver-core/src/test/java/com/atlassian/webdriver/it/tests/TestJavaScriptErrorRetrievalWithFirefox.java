package com.atlassian.webdriver.it.tests;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.IncludedScriptErrorPage;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;

/**
 * Test the rule for logging client-side console output.
 *
 * At the time of writing, Firefox does not support this. This test will fail if Firefox is able to retrieve logs.
 * In this case, the documentation should be updated to reflect this change and a proper test should be implemented.
 */
@RequireBrowser(Browser.FIREFOX)
public class TestJavaScriptErrorRetrievalWithFirefox extends AbstractSimpleServerTest
{

    private JavaScriptErrorsRule rule;

    @Before
    public void setUp()
    {
        rule = new JavaScriptErrorsRule();
    }

    @Test
    public void testNoErrorsRetrieved()
    {
        product.visit(IncludedScriptErrorPage.class);

        String consoleOutput = rule.getConsoleOutput();

        assertThat(consoleOutput, isEmptyString());
    }
}
