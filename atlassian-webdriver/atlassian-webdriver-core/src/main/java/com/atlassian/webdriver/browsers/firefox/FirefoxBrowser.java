package com.atlassian.webdriver.browsers.firefox;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.webdriver.browsers.profile.ProfilePreferences;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static com.atlassian.webdriver.WebDriverFactory.isBrowserHeadless;
import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;

/**
 * A helper utility for obtaining a FirefoxDriver.
 * @since 2.0
 */
public final class FirefoxBrowser
{
    private static final Logger log = LoggerFactory.getLogger(FirefoxBrowser.class);

    private FirefoxBrowser()
    {
        throw new IllegalStateException("FirefoxBrowser is not constructable");
    }

    /**
     * Gets the default FirefoxDriver that tries to use the default system paths
     * for where the firefox binary should be
     * @return Default configured FirefoxDriver
     */
    public static FirefoxDriver getFirefoxDriver()
    {
        final GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();

        setSystemProperties(firefoxBuilder);
        return constructFirefoxDriver(firefoxBuilder, null);
    }

    /**
     * Configures a FirefoxDriver based on the browserConfig
     * @param browserConfig browser config that points to the binary path of the browser and
     * optional profile
     * @return A configured FirefoxDriver based on the browserConfig passed in
     */
    public static FirefoxDriver getFirefoxDriver(BrowserConfig browserConfig)
    {
        GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();

        if (browserConfig != null)
        {
            firefoxBuilder.usingFirefoxBinary(new FirefoxBinary(new File(browserConfig.getBinaryPath())));

            FirefoxProfile profile = null;
            if (browserConfig.getProfilePath() != null)
            {
                File profilePath = new File(browserConfig.getProfilePath());

                profile = new FirefoxProfile();
                addExtensionsToProfile(profile, profilePath);
                addPreferencesToProfile(profile, profilePath);
                addGeckoDriver(firefoxBuilder, profilePath);
            }

            setSystemProperties(firefoxBuilder);
            return constructFirefoxDriver(firefoxBuilder, profile);
        }

        // Fall back on default firefox driver
        return getFirefoxDriver();
    }

    /**
     * Add command line options if provided via system property {@literal webdriver.firefox.switches}.
     *
     */
    private static void addCommandLine(final FirefoxOptions options)
    {
        String[] switches = StringUtils.split(System.getProperty("webdriver.firefox.switches"), ",");
        if(switches != null && switches.length > 0) {
            List<String> switchList = Arrays.asList(switches);
            log.info("Setting command line arguments for Firefox: " + switchList);
            options.addArguments(switchList);
        }
    }

    private static void addGeckoDriver(GeckoDriverService.Builder firefoxBuilder, File profilePath)
    {
        if (profilePath != null) {
            File geckoDriverFile = new File(profilePath, "geckodriver" + (IS_OS_WINDOWS ? ".exe" : ""));
            if (geckoDriverFile.exists()) {
                firefoxBuilder.usingDriverExecutable(new File(geckoDriverFile.toString()));
            }
        }
    }

    private static void addPreferencesToProfile(FirefoxProfile profile, File profilePath)
    {
        File profilePreferencesFile = new File(profilePath, "profile.preferences");

        if (profilePreferencesFile.exists())
        {
            ProfilePreferences profilePreferences = new ProfilePreferences(profilePreferencesFile);
            Map<String, Object> preferences = profilePreferences.getPreferences();
            for (String key : preferences.keySet())
            {
                Object value = preferences.get(key);
                if (value instanceof Integer)
                {
                    profile.setPreference(key, (Integer) value);
                }
                else if (value instanceof Boolean)
                {
                    profile.setPreference(key, (Boolean) value);
                }
                else
                {
                    profile.setPreference(key, (String) value);
                }
            }
        }
    }

    private static void addExtensionsToProfile(FirefoxProfile profile, File profilePath)
    {
        // Filter the extentions path to only include extensions.
        for (File extension : profilePath.listFiles(file -> {
            if (file.getName().matches(".*\\.xpi$"))
            {
                return true;
            }

                return false;
            }))
        {
            profile.addExtension(extension);
        }
    }

    /**
     * Gets a firefox driver based on the browser path based in
     * @param browserPath the path to the firefox binary to use for the firefox driver.
     * @return A FirefoxDriver that is using the binary at the browserPath
     */
    public static FirefoxDriver getFirefoxDriver(String browserPath)
    {
        if (browserPath != null)
        {
            final GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();
            firefoxBuilder.usingFirefoxBinary(new FirefoxBinary(new File(browserPath)));
            setSystemProperties(firefoxBuilder);
            return constructFirefoxDriver(firefoxBuilder, null);
        }

        // Fall back on default firefox driver
        log.info("Browser path was null, falling back to default firefox driver.");
        return getFirefoxDriver();
    }

    public static void updateOptions(FirefoxOptions options) {
        options.setHeadless(isBrowserHeadless());

        LoggingPreferences loggingPreferences = new LoggingPreferences();
        loggingPreferences.enable(LogType.BROWSER, Level.SEVERE);
        options.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
    }

    private static FirefoxDriver constructFirefoxDriver(final GeckoDriverService.Builder firefoxBuilder, FirefoxProfile profile)
    {
        if (profile == null)
        {
            profile = new FirefoxProfile();
        }

        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);
        addCommandLine(options);
        updateOptions(options);
        showEnv();

        return new FirefoxDriver(firefoxBuilder.build(), options);
    }

    private static void showEnv()
    {
        System.out.println("==== System properties ====");
        System.out.println(System.getProperties());
    }

    /**
     * Sets up system properties on the firefox builder.
     *
     * @param builder
     */
    private static void setSystemProperties(GeckoDriverService.Builder builder)
    {
        System.out.println("==== Setting system properties ====");
        System.out.println("Display: " + System.getProperty("DISPLAY"));
        if (System.getProperty("DISPLAY") != null)
        {
            builder.withEnvironment(ImmutableMap.of("DISPLAY", System.getProperty("DISPLAY")));
        }
        System.setProperty("webdriver.http.factory", "apache");
    }
}
