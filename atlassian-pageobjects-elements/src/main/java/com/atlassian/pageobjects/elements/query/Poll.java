package com.atlassian.pageobjects.elements.query;

import com.atlassian.pageobjects.elements.query.util.Backoff;
import com.atlassian.pageobjects.elements.query.util.LinearBackoff;
import com.google.common.annotations.VisibleForTesting;
import org.hamcrest.Matcher;

import java.time.Clock;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.notNullValue;

public class Poll<T> implements Callable<T>
{
    Callable<T> query;
    Matcher<? super T> condition;
    Backoff interval;
    long timeout;
    Clock clock;
    ExpirationHandler expirationHandler;

    @VisibleForTesting
    public Poll(Callable<T> query, Clock clock)
    {
        this(query);
        this.clock = clock;
    }

    private Poll(Callable<T> query)
    {
        this.query = query;
        this.condition = notNullValue();
        this.interval = new LinearBackoff(50);
        this.timeout = 10000;
        this.clock = Clock.systemUTC();
        this.expirationHandler = ExpirationHandler.RETURN_CURRENT;
    }

    public static <T> Poll<T> poll(Callable<T> query)
    {
        return new Poll<T>(query);
    }

    public Poll<T> every(long interval, TimeUnit unit)
    {
        this.interval = new LinearBackoff(TimeUnit.MILLISECONDS.convert(interval, unit));
        return this;
    }

    public Poll<T> until(Matcher<? super T> condition)
    {
        this.condition = condition;
        return this;
    }

    public Poll<T> withTimeout(long time, TimeUnit unit)
    {
        if (time < 0)
        {
            throw new IllegalArgumentException("Timeout must be a positive value.");
        }
        this.timeout = TimeUnit.MILLISECONDS.convert(time, unit);
        return this;
    }

    public Poll<T> onFailure(ExpirationHandler expirationHandler)
    {
        this.expirationHandler = expirationHandler;
        return this;
    }


    public T call() throws RuntimeException
    {
        try
        {
            final long start = clock.millis();
            final long deadline = start + timeout;
            T current;

            while (clock.millis() < deadline)
            {
                current = query.call();
                if (condition.matches(current))
                {
                    return current;
                }
                else
                {
                    long bounded = Math.max(1, deadline - clock.millis());
                    interval.yield(bounded);
                }
            }
            interval.resetBackoff();

            current = query.call();
            if (condition.matches(current))
            {
                return current;
            }

            return expirationHandler.expired(query.toString(), current, timeout);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
