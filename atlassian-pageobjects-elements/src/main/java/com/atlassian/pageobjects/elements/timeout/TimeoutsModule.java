package com.atlassian.pageobjects.elements.timeout;

import com.google.inject.Binder;
import com.google.inject.Module;

import static java.util.Objects.requireNonNull;

/**
 * A module implementation to provide {@link com.atlassian.pageobjects.elements.timeout.Timeouts} implementation
 * to the injector.
 */
public class TimeoutsModule implements Module
{
    private final Timeouts timeouts;

    public TimeoutsModule()
    {
        this(new DefaultTimeouts());
    }

    public TimeoutsModule(final Timeouts timeouts)
    {
        this.timeouts = requireNonNull(timeouts, "Timeouts can't be null");
    }

    public void configure(final Binder binder)
    {
        binder.bind(Timeouts.class).toInstance(timeouts);
    }

    public Timeouts timeouts()
    {
        return timeouts;
    }
}
