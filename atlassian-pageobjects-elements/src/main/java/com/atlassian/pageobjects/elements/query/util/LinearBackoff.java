package com.atlassian.pageobjects.elements.query.util;

public class LinearBackoff implements Backoff
{
    private long interval;

    public LinearBackoff(long interval) {
        this.interval = interval;
    }

    @Override
    public void yield() throws InterruptedException
    {
        if(interval > 0)
        {
            Thread.sleep(interval);
        }
    }

    @Override
    public void yield(long bound) throws InterruptedException
    {
        if(interval > 0)
        {
            Thread.sleep(Math.min(bound, interval));
        }
    }

    @Override
    public void resetBackoff()
    {

    }
}
