package com.atlassian.pageobjects.elements.mock.clock;

import javax.annotation.concurrent.NotThreadSafe;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * A {@link Clock} implementation that periodically increments the returned value.
 *
 */
@NotThreadSafe
public class PeriodicClock extends AbstractMockClock
{
    private final Duration increment;
    private final int periodLength;

    private Instant current;
    private int currentInPeriod = 0;

    public PeriodicClock(Instant intitialValue, Duration increment, int periodLength)
    {
        checkArgument(!increment.isNegative(), "increment should positive");
        checkArgument(periodLength > 0, "periodLength should be > 0");
        this.current = intitialValue;
        this.increment = increment;
        this.periodLength = periodLength;
    }

    public PeriodicClock(Duration increment, int periodLength)
    {
        this(Instant.EPOCH, increment, periodLength);
    }

    public PeriodicClock(Duration increment)
    {
        this(Instant.EPOCH, increment, 1);
    }



    @Override
    public Instant instant() {
        Instant answer = current;
        if (periodFinished())
        {
            current = current.plus(increment);
        }
        return answer;
    }

    private boolean periodFinished()
    {
        currentInPeriod++;
        if (currentInPeriod == periodLength)
        {
            currentInPeriod = 0;
            return true;
        }
        return false;
    }
}
