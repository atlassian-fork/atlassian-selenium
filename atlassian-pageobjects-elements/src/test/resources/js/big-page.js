document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('button-right').addEventListener('click', function () {
        var span = document.createElement('span');
        span.setAttribute('id', 'button-right-clicked');
        span.appendChild(document.createTextNode('Right button clicked!'));
        document.getElementById('messages').appendChild(span);
    });
    document.getElementById('button-bottom').addEventListener('click', function () {
        var span = document.createElement('span');
        span.setAttribute('id', 'button-bottom-clicked');
        span.appendChild(document.createTextNode('Bottom button clicked!'));
        document.getElementById('messages').appendChild(span);
    });
});